package com.castle.pdftools.demo.controller;

import com.castle.pdftools.demo.entity.RespBody;
import com.castle.pdftools.utils.FileUploaderUtils;
import com.castle.pdftools.utils.PDFCompressUtils;
import com.castle.pdftools.utils.PdfUtils;
import com.castle.pdftools.utils.SvgUils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件转 PDF 示例
 *
 * @author hcwdc.com
 */
@RestController
@RequestMapping("/docpreview")
public class PdfToolsDemo {
    @Autowired
    private FileUploaderUtils fileUploaderUtils;
    @Autowired
    private PdfUtils pdfUtils;

    /**
     * 文件上传
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("upload")
    public RespBody<Map<String, Object>> upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return RespBody.fail(-1, "请上传文件");
        }
        String fileName = file.getOriginalFilename();
        String urlFileName = fileUploaderUtils.getRandomFileName(FileUploaderUtils.getSuffix(fileName));
        String url = fileUploaderUtils.upload(file.getBytes(), urlFileName);
        Map<String, Object> data = new HashMap<>(1);
        data.put("src", url);
        return RespBody.data(data);
    }


    /**
     * 文件上传并转为PDF
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("topdf")
    public RespBody<Map<String, Object>> toPdf(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return RespBody.fail(-1, "请上传文件");
        }
        String fileName = file.getOriginalFilename();
        String urlFileName = fileUploaderUtils.getRandomFileName(FileUploaderUtils.getSuffix(fileName));
        String originalUrl = fileUploaderUtils.upload(file.getBytes(), urlFileName);
        pdfUtils.toPdf(pdfUtils.getServerPath(originalUrl), pdfUtils.getTargetFolder(originalUrl));
        String pdfPath = pdfUtils.getServerPath(pdfUtils.getPDFUrl(originalUrl));
        int dot = pdfPath.lastIndexOf('.');
        String compressPath = pdfPath.substring(0, dot) + "_compress.pdf";
        String compressUrl = originalUrl.substring(0, originalUrl.lastIndexOf(".")) + "_compress.pdf";
        PDFCompressUtils pdfCompressUtils = new PDFCompressUtils();
        pdfCompressUtils.setInput(new File(pdfPath));
        pdfCompressUtils.setOutput(new File(compressPath));
        pdfCompressUtils.runShell();
        Map<String, Object> data = new HashMap<>();
        data.put("src", originalUrl);
        data.put("pdfPath", pdfUtils.getPDFUrl(originalUrl));
        data.put("compressPDF", compressUrl);
        return RespBody.data(data);
    }

    /**
     * 文件上传并转为图片PNG格式
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("topng")
    public RespBody<Map<String, Object>> toPng(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return RespBody.fail(-1, "请上传文件");
        }
        String fileName = file.getOriginalFilename();
        String urlFileName = fileUploaderUtils.getRandomFileName(FileUploaderUtils.getSuffix(fileName));
        String originalUrl = fileUploaderUtils.upload(file.getBytes(), urlFileName);
        pdfUtils.toPdf(pdfUtils.getServerPath(originalUrl), pdfUtils.getTargetFolder(originalUrl));

        int page = pdfUtils.pdf2Image(pdfUtils.getServerPath(pdfUtils.getPDFUrl(originalUrl)), pdfUtils.getTargetFolder(originalUrl), 96);
        Map<String, Object> data = new HashMap<>();
        data.put("src", originalUrl);
        data.put("pdfPath", pdfUtils.getPDFUrl(originalUrl));
        data.put("pngNum", page);
        data.put("pngList", pdfUtils.getPngUrl(originalUrl, page));
        return RespBody.data(data);
    }

    /**
     * 文件上传并转为图片PNG格式
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("tosvg")
    public RespBody<Map<String, Object>> toSvg(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return RespBody.fail(-1, "请上传文件");
        }
        String fileName = file.getOriginalFilename();
        String urlFileName = fileUploaderUtils.getRandomFileName(FileUploaderUtils.getSuffix(fileName));
        String originalUrl = fileUploaderUtils.upload(file.getBytes(), urlFileName);
        pdfUtils.toPdf(pdfUtils.getServerPath(originalUrl), pdfUtils.getTargetFolder(originalUrl));
        String pdfPath = pdfUtils.getServerPath(pdfUtils.getPDFUrl(originalUrl));
        int dot = pdfPath.lastIndexOf('.');
        int lastSep = pdfPath.lastIndexOf(File.separator) + 1;
        String imagePDFName = pdfPath.substring(lastSep, dot); // 获取图片文件名
        String svgPath = pdfUtils.getTargetFolder(originalUrl) + imagePDFName + "_";
        int page = SvgUils.pdfToSvg(pdfPath, svgPath);
        Map<String, Object> data = new HashMap<>();
        data.put("src", originalUrl);
        data.put("pdfPath", pdfPath);
        data.put("pngNum", page);
        data.put("pngList", pdfUtils.getSvgUrl(originalUrl, page));
        return RespBody.data(data);
    }

    /**
     * PDF压缩
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("PDFCompress")
    public RespBody<Map<String, Object>> PDFCompress(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return RespBody.fail(-1, "请上传文件");
        }
        String fileName = file.getOriginalFilename();
        String urlFileName = fileUploaderUtils.getRandomFileName(FileUploaderUtils.getSuffix(fileName));
        String originalUrl = fileUploaderUtils.upload(file.getBytes(), urlFileName);
        pdfUtils.toPdf(pdfUtils.getServerPath(originalUrl), pdfUtils.getTargetFolder(originalUrl));
        String pdfPath = pdfUtils.getServerPath(pdfUtils.getPDFUrl(originalUrl));
        PDFCompressUtils pdfCompressUtils = new PDFCompressUtils();
        pdfCompressUtils.setInput(new File(pdfPath));
        int dot = pdfPath.lastIndexOf('.');
        int lastSep = pdfPath.lastIndexOf(File.separator) + 1;
        String imagePDFName = pdfPath.substring(lastSep, dot); // 获取图片文件名
        System.out.println(pdfPath.substring(0, dot));
        pdfCompressUtils.setOutput(new File(pdfPath));
        return RespBody.data(null);
    }
}
